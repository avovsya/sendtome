$(function () {
  var isiDevice = /ipad|iphone|ipod/i.test(navigator.userAgent.toLowerCase());
  var isAndroid = /android/i.test(navigator.userAgent.toLowerCase());
  var isWindowsPhone = /windows phone/i.test(navigator.userAgent.toLowerCase());
   
  if (isiDevice || isAndroid || isWindowsPhone)
  {
    $('.panel-receive').detach().prependTo('.panel-container');
  }
  $('.btn.send').click(function (e) {
    var payload = $('#payload').val();
    if (!payload || payload.length === 0) {
      alert('Enter link or text before sending');
      return;
    }
    $.post('/send', { payload: payload }, function (result) {
      if (result.pin) {
        // show pin
        $('.send').hide();
        $('.sent .result').html(result.pin);
        $('.sent').show();
      } else if (result.error) {
        showError(result.error);
      } else {
        showError('Something went wrong on our server. Sorry');
      }
    });
  });

  $('.btn.receive').click(function (e) {
    var pin = $('#pin').val();
    if (!pin || pin.length === 0) {
      alert('You must enter PIN code to receive link(or text)');
      return;
    }
    $.post('/receive', { pin: pin }, function (result) {
      if (result.payload) {
        var text = result.payload.replace(/(?:(https?\:\/\/[^\s]+))/m, '<a target="_blank" href="$1">$1</a>');
        $('.receive').hide();
        $('.received .result').html(text);
        $('.received').show();
        // show payload
      } else if (result.error) {
        showError(result.error);
      } else {
        showError('Something went wrong on our server. Sorry');
      }
    });
  });

  $('.sent .btn.another').click(function () {
    $('.sent').hide();
    $('.send').show();
  });

  $('.received .btn.another').click(function () {
    $('.received').hide();
    $('.receive').show();
  });
});

function showError(message) {
  $('.error').last().clone().appendTo('.error-container').html(message).show();
}
