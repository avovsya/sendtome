// TODO: payload size limit
// TODO: SEO
// TODO: Reduce page paddings on mobile devices
// TODO: add contact data on main page
// DONE: limit number of requests from one IP(ddos and brutforece protection)
// DONE: Show error list('no such pin', 'too big payload', etc)
// DONE: highlight links in payload(when showing it to receiver)
var express = require('express');
var redisLib = require('redis');
var redis = redisLib.createClient(10047, 'pub-redis-10047.us-east-1-3.3.ec2.garantiadata.com', { auth_pass: 'superduperpasswordformydb24_38' });

var EXPIRE_TIME = 5 * 60; // pin code will expire in 5 minutes

exports.indexView = function indexView(req, res, next) {
  res.render('index');
};

exports.send = function post(req, res, next) {
  if (req.body.payload.length > 2000) {
    return res.send({ error: 'Text is to big' });
  }
  generateRandomPin(function (err, pin) {
    if (err) {
      return next(err);
    }
    redis.set(pin, req.body.payload, function (err) {
      if (err) {
        return next(err);
      }
      redis.expire(pin, EXPIRE_TIME, function (err) {
        if (err) {
          return next(err);
        }
        return res.send({ pin: pin });
      });
    });
  });
};

exports.receive = function post(req, res, next) {
  var pin = req.body.pin;
  redis.get(pin, function (err, result) {
    if (err) {
      return next(err);
    }
    if (result) {
      return res.send({ payload: result });
    }
    else {
      return res.send({ error: 'PIN not found' });
    }
  });
};

var generateRandomPin = function generateRandomPin(cb) {
  var pin = '';
  for( var i=0; i < 6; i++ ) {
    pin += Math.floor(Math.random() * 10).toString();
  }
  redis.exists(pin, function (err, res) {
    if (err) {
      return cb(err);
    }
    if (res === 0) {
      return cb(null, pin);
    } else {
      return generateRandomPin(cb);
    }
  });
};
